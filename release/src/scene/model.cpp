/**
 * @file model.cpp
 * @brief Model class
 *
 * @author Eric Butler (edbutler)
 * @author Zeyang Li (zeyangl)
 */

#include "scene/model.hpp"
#include "scene/material.hpp"
#include "application/opengl.hpp"
#include "scene/triangle.hpp"
#include "p3/raytracer.hpp"
#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#define SQUARE_MAX_DISTANCE 100000000;

namespace _462 {

Model::Model() : mesh( 0 ), material( 0 ) { }
Model::~Model() { }

/****** stkuo added ******/

GeoType Model::getGeoType(){
	return GeoType::MODEL;
}


void Model::getIntersec(Ray r,Intersection& intersec){
        intersec.hit = false;
	intersec.t = 0;
	intersec.hitPoint_world = Vector3::Zero();
	intersec.hitNormal_world = Vector3::Zero();
	intersec.hitUVCoord = Vector2::Zero();
        Ray convertedRay;
	convertedRay.e = (this->invMat * Vector4(r.e, 1)).xyz();
	convertedRay.d = (this->invMat * Vector4(r.d, 0)).xyz();
	real_t t = -1;
	real_t sqr_minDistance = SQUARE_MAX_DISTANCE;
	const MeshTriangle* triangleList;
	const MeshVertex* vertexList;
	if(this->mesh->num_triangles() != 0){
		triangleList = this->mesh->get_triangles();
		vertexList = this->mesh->get_vertices();
		Vector3 point, bary_coord;
		Vector2 uv_coord;
		size_t i, firstHitID = 0;
		for(i = 0; i<this->mesh->num_triangles(); i++){
			MeshVertex v0, v1, v2;
			Vector3 triNormal;
			v0 = vertexList[triangleList[i].vertices[0]];
			v1 = vertexList[triangleList[i].vertices[1]];
			v2 = vertexList[triangleList[i].vertices[2]];
			triNormal = normalize(cross((v1.position-v0.position), (v2.position-v0.position)));
			// plane formula:  dot(((e+dt)-v0), triNormal) = 0
			real_t denominator, numerator;
			denominator = -dot(convertedRay.d, triNormal);
        		if(denominator == 0) return;
        		numerator = dot((convertedRay.e-v0.position),triNormal);
			t = numerator/denominator;
			//check if it is on plane
			point = convertedRay.e + (t*convertedRay.d);
			if(insideTriangle(point, v0.position, v1.position, v2.position)){
				//if(dot(convertedRay.d, triNormal) < 0){  //correct normal direction
					real_t sqr_tempDistance = squared_length(point - convertedRay.e);
					if(sqr_tempDistance < sqr_minDistance){
						intersec.hit = true;
						intersec.t = t;		
						firstHitID = i;
						intersec.hitPoint_world = (this->mat * Vector4(point, 1)).xyz();
						sqr_minDistance = sqr_tempDistance;
					}
				//}
			}
		}
		MeshVertex final_v0, final_v1, final_v2;
		final_v0 = vertexList[triangleList[firstHitID].vertices[0]];
		final_v1 = vertexList[triangleList[firstHitID].vertices[1]];
		final_v2 = vertexList[triangleList[firstHitID].vertices[2]];
		if(intersec.hit == true){
			point = convertedRay.e + (intersec.t*convertedRay.d);
			getBarycentric(point, final_v0.position, final_v1.position, final_v2.position, bary_coord);
			intersec.hitNormal_world = this->normMat * 
						 normalize( bary_coord.x * final_v0.normal
							  + bary_coord.y * final_v1.normal
							  + bary_coord.z * final_v2.normal);
			intersec.hitUVCoord = bary_coord.x * final_v0.tex_coord
					    + bary_coord.y * final_v1.tex_coord
					    + bary_coord.z * final_v2.tex_coord;
			intersec.rIndex = this->material->refractive_index;
			intersec.specular = this->material->specular;
			intersec.ambient = this->material->ambient;
			intersec.diffuse = this->material->diffuse;
			intersec.texture_color = this->material->texture.sample(intersec.hitUVCoord);
		}
	}
        return;
}

/***** end of stkuo  *****/


void Model::render() const
{
    if ( !mesh )
        return;
    if ( material )
        material->set_gl_state();
    mesh->render();
    if ( material )
        material->reset_gl_state();
}
bool Model::initialize(){
    Geometry::initialize();
    return true;
}

} /* _462 */
