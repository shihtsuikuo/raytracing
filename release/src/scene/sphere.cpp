/**
 * @file sphere.cpp
 * @brief Function defnitions for the Sphere class.
 *
 * @author Kristin Siu (kasiu)
 * @author Eric Butler (edbutler)
 */

#include "scene/sphere.hpp"
#include "application/opengl.hpp"
#include <algorithm>

namespace _462 {

#define SPHERE_NUM_LAT 80
#define SPHERE_NUM_LON 100

#define SPHERE_NUM_VERTICES ( ( SPHERE_NUM_LAT + 1 ) * ( SPHERE_NUM_LON + 1 ) )
#define SPHERE_NUM_INDICES ( 6 * SPHERE_NUM_LAT * SPHERE_NUM_LON )
// index of the x,y sphere where x is lat and y is lon
#define SINDEX(x,y) ((x) * (SPHERE_NUM_LON + 1) + (y))
#define VERTEX_SIZE 8
#define TCOORD_OFFSET 0
#define NORMAL_OFFSET 2
#define VERTEX_OFFSET 5
#define BUMP_FACTOR 1

static unsigned int Indices[SPHERE_NUM_INDICES];
static float Vertices[VERTEX_SIZE * SPHERE_NUM_VERTICES];

static void init_sphere()
{
    static bool initialized = false;
    if ( initialized )
        return;

    for ( int i = 0; i <= SPHERE_NUM_LAT; i++ ) {
        for ( int j = 0; j <= SPHERE_NUM_LON; j++ ) {
            real_t lat = real_t( i ) / SPHERE_NUM_LAT;
            real_t lon = real_t( j ) / SPHERE_NUM_LON;
            float* vptr = &Vertices[VERTEX_SIZE * SINDEX(i,j)];

            vptr[TCOORD_OFFSET + 0] = lon;
            vptr[TCOORD_OFFSET + 1] = 1-lat;

            lat *= PI;
            lon *= 2 * PI;
            real_t sinlat = sin( lat );

            vptr[NORMAL_OFFSET + 0] = vptr[VERTEX_OFFSET + 0] = sinlat * sin( lon );
            vptr[NORMAL_OFFSET + 1] = vptr[VERTEX_OFFSET + 1] = cos( lat ),
            vptr[NORMAL_OFFSET + 2] = vptr[VERTEX_OFFSET + 2] = sinlat * cos( lon );
        }
    }

    for ( int i = 0; i < SPHERE_NUM_LAT; i++ ) {
        for ( int j = 0; j < SPHERE_NUM_LON; j++ ) {
            unsigned int* iptr = &Indices[6 * ( SPHERE_NUM_LON * i + j )];

            unsigned int i00 = SINDEX(i,  j  );
            unsigned int i10 = SINDEX(i+1,j  );
            unsigned int i11 = SINDEX(i+1,j+1);
            unsigned int i01 = SINDEX(i,  j+1);

            iptr[0] = i00;
            iptr[1] = i10;
            iptr[2] = i11;
            iptr[3] = i11;
            iptr[4] = i01;
            iptr[5] = i00;
        }
    }

    initialized = true;
}

Sphere::Sphere()
    : radius(0), material(0) {}

Sphere::~Sphere() {}

/***** stkuo added  ******/
GeoType Sphere::getGeoType(){
	return GeoType::SPHERE;
}

Vector2 Sphere::getUVCoord(Vector3& point, const real_t& radius){
	Vector2 uv;
	real_t phi, theta;
	Vector3 unit_point = normalize(point);
	phi = acos(-dot(Vector3::UnitY(), unit_point));
	uv.y = phi/PI;
	theta = ( acos( dot(unit_point, Vector3::UnitZ())/sin(phi) )) / (2*PI);
	if( dot( cross(Vector3::UnitY(), Vector3::UnitZ()), unit_point) > 0)
		uv.x = theta;
	else
		uv.x= 1-theta;
	return uv;
}

void Sphere::getIntersec(Ray r, Intersection& intersec){
	Ray convertedRay;
	convertedRay.e = (this->invMat * Vector4(r.e, 1)).xyz();
	convertedRay.d = (this->invMat * Vector4(r.d, 0)).xyz();
	real_t A = squared_length(convertedRay.d);
	real_t B = 2*dot(convertedRay.e, convertedRay.d);
	real_t C = squared_length(convertedRay.e) - (this->radius*this->radius);
	real_t D = B*B - 4*A*C;
	if(D > 0){
		intersec.hit = true;
		real_t tempt1, tempt2;
		tempt1 = ((-B)+sqrt(D))/(2*A);
		tempt2 = ((-B)-sqrt(D))/(2*A);
		Vector3 tempp1, tempp2;
		tempp1 = convertedRay.e + (convertedRay.d*tempt1);
		tempp2 = convertedRay.e + (convertedRay.d*tempt2);
		//if(dot(tempp1, convertedRay.d) < 0){
		if(squared_length(tempp1-convertedRay.e) < squared_length(tempp2-convertedRay.e)){
			intersec.t = tempt1;
			intersec.hitPoint_world = (this->mat * Vector4(tempp1, 1)).xyz();
			intersec.hitNormal_world = (this->normMat * normalize(tempp1));
			//intersec.hitNormal_world = (this->normMat * tempp1);
			intersec.hitUVCoord = getUVCoord(tempp1, this->radius);
		}else{
			intersec.t = tempt2;
			intersec.hitPoint_world = (this->mat * Vector4(tempp2, 1)).xyz();
			intersec.hitNormal_world = (this->normMat * normalize(tempp2));
			//intersec.hitNormal_world = (this->normMat * tempp2);
			intersec.hitUVCoord = getUVCoord(tempp2, this->radius);
		}
			intersec.rIndex = this->material->refractive_index;
			intersec.specular = this->material->specular;
			intersec.ambient = this->material->ambient;
			intersec.diffuse = this->material->diffuse;
			intersec.texture_color = this->material->texture.sample(intersec.hitUVCoord);
		return;
	}else if(D == 0){
		intersec.hit = true;
		real_t tempt = (-B)/(2*A);
		Vector3 tempp = convertedRay.e + (convertedRay.d*tempt);
		intersec.t = tempt;
		intersec.hitPoint_world = (this->mat * Vector4(tempp, 1)).xyz();
		//intersec.hitNormal_world = (this->normMat * tempp);
		intersec.hitNormal_world = (this->normMat * normalize(tempp));
		intersec.hitUVCoord = getUVCoord(tempp, this->radius);
		intersec.rIndex = this->material->refractive_index;
		intersec.specular = this->material->specular;
		intersec.ambient = this->material->ambient;
		intersec.diffuse = this->material->diffuse;
		intersec.texture_color = this->material->texture.sample(intersec.hitUVCoord);
		return;
	} 
	
	intersec.hit = false;
	intersec.t = 0;
	intersec.hitPoint_world = Vector3::Zero();
	intersec.hitNormal_world = Vector3::Zero();
	return;
}


/***** end of stkuo  *****/
void Sphere::render() const
{
    // create geometry if we haven't already
    init_sphere();

    if ( material )
        material->set_gl_state();

    // just scale by radius and draw unit sphere
    glPushMatrix();
    glScaled( radius, radius, radius );
    glInterleavedArrays( GL_T2F_N3F_V3F, VERTEX_SIZE * sizeof Vertices[0], Vertices );
    glDrawElements( GL_TRIANGLES, SPHERE_NUM_INDICES, GL_UNSIGNED_INT, Indices );
    glPopMatrix();

    if ( material )
        material->reset_gl_state();
}


//quadratic formula
//If a solution exists, returns answers in x1 and x2, and returns true.
//Otherwise, returns false
bool solve_quadratic(real_t *x1,real_t *x2, real_t a, real_t b, real_t c){
    real_t b24ac = b*b-4*a*c;
    if(b24ac<0){
        return false;
    }else{
        real_t sb24ac=sqrt(b24ac);
        *x1=(-b+sb24ac)/(2*a);
        *x2=(-b-sb24ac)/(2*a);
        return true;
    }
}
//solve a quadratic equation, and then return the smallest solution larger than EPS
//if there is no solution, return -1
real_t solve_time(real_t a,real_t b,real_t c){
    real_t x1;
    real_t x2;
    if(solve_quadratic(&x1,&x2,a,b,c)){
        if(x1>EPS && x2>EPS){
            return std::min(x1,x2);
        }else if(x1>EPS){
            return x1;
        }else if(x2>EPS){
            return x2;
        }
    }
    return -1;
}
} /* _462 */

