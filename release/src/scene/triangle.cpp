/**
 * @file triangle.cpp
 * @brief Function definitions for the Triangle class.
 *
 * @author Eric Butler (edbutler)
 */

#include "scene/triangle.hpp"
#include "application/opengl.hpp"
#include "math/math.hpp"
#include "p3/raytracer.hpp"

namespace _462 {

Triangle::Triangle()
{
    vertices[0].material = 0;
    vertices[1].material = 0;
    vertices[2].material = 0;
    isBig=true;
}

Triangle::~Triangle() { }

/***** stkuo added ******/

bool insideTriangle(const Vector3& point, const Vector3& va, const Vector3& vb, const Vector3& vc){
        if(dot( cross(vb-va, point-va),  cross(vb-va, vc-va)) < 0) 
                return false;
        if(dot( cross(vc-vb, point-vb),  cross(vc-vb, va-vb)) < 0)
                return false; 
        if(dot( cross(va-vc, point-vc),  cross(va-vc, vb-vc)) < 0)
                return false; 
        return true;
}

GeoType Triangle::getGeoType(){
	return GeoType::TRIANGLE;
}

void Triangle::getIntersec(Ray r, Intersection& intersec){
        intersec.hit = false;
	intersec.t = 0;
	intersec.hitPoint_world = Vector3::Zero();
	intersec.hitNormal_world = Vector3::Zero();
        Ray convertedRay;
        convertedRay.e = (this->invMat * Vector4(r.e, 1)).xyz();
        convertedRay.d = (this->invMat * Vector4(r.d, 0)).xyz();
        real_t t;
	Vector3 triNormal, point;
	Vertex v0, v1, v2;
	v0 = vertices[0];
	v1 = vertices[1];
	v2 = vertices[2];
	triNormal = normalize(cross((v1.position-v0.position), (v2.position-v0.position)));
	// plane formula:  dot(((e+dt)-v0), triNormal) = 0
	real_t denominator, numerator;
	denominator = -dot(convertedRay.d, triNormal);
	if(denominator == 0) return;
	numerator = dot((convertedRay.e-v0.position),triNormal);
	t = numerator/denominator;
	//check if it is on plane
	point = convertedRay.e + (t*convertedRay.d);
	if(insideTriangle(point, v0.position, v1.position, v2.position)){
			intersec.hit = true;
			intersec.t = t;
			intersec.hitPoint_world = (this->mat * Vector4(point, 1)).xyz();
                        Vector3 bary_coord;
			getBarycentric(point, v0.position, v1.position, v2.position, bary_coord);
			intersec.hitNormal_world = this->normMat *
						 normalize( bary_coord.x * v0.normal
						 + bary_coord.y * v1.normal
						 + bary_coord.z * v2.normal);
			intersec.hitUVCoord = bary_coord.x * v0.tex_coord
					    + bary_coord.y * v1.tex_coord
					    + bary_coord.z * v2.tex_coord;
			intersec.rIndex = bary_coord.x * v0.material->refractive_index
					+ bary_coord.y * v1.material->refractive_index
					+ bary_coord.z * v2.material->refractive_index;
			intersec.specular = bary_coord.x * v0.material->specular
					  + bary_coord.y * v1.material->specular
					  + bary_coord.z * v2.material->specular;
			intersec.ambient = bary_coord.x * v0.material->ambient
					 + bary_coord.y * v1.material->ambient
					 + bary_coord.z * v2.material->ambient;
			intersec.diffuse = bary_coord.x * v0.material->diffuse
					 + bary_coord.y * v1.material->diffuse
					 + bary_coord.z * v2.material->diffuse;
			intersec.texture_color = bary_coord.x * v0.material->texture.sample(intersec.hitUVCoord)
					       + bary_coord.y * v1.material->texture.sample(intersec.hitUVCoord)
					       + bary_coord.z * v2.material->texture.sample(intersec.hitUVCoord);

			
	}

        return;

}


/***** end of stkuo  *****/

void Triangle::render() const
{
    bool materials_nonnull = true;
    for ( int i = 0; i < 3; ++i )
        materials_nonnull = materials_nonnull && vertices[i].material;

    // this doesn't interpolate materials. Ah well.
    if ( materials_nonnull )
        vertices[0].material->set_gl_state();

    glBegin(GL_TRIANGLES);

#if REAL_FLOAT
    glNormal3fv( &vertices[0].normal.x );
    glTexCoord2fv( &vertices[0].tex_coord.x );
    glVertex3fv( &vertices[0].position.x );

    glNormal3fv( &vertices[1].normal.x );
    glTexCoord2fv( &vertices[1].tex_coord.x );
    glVertex3fv( &vertices[1].position.x);

    glNormal3fv( &vertices[2].normal.x );
    glTexCoord2fv( &vertices[2].tex_coord.x );
    glVertex3fv( &vertices[2].position.x);
#else
    glNormal3dv( &vertices[0].normal.x );
    glTexCoord2dv( &vertices[0].tex_coord.x );
    glVertex3dv( &vertices[0].position.x );

    glNormal3dv( &vertices[1].normal.x );
    glTexCoord2dv( &vertices[1].tex_coord.x );
    glVertex3dv( &vertices[1].position.x);

    glNormal3dv( &vertices[2].normal.x );
    glTexCoord2dv( &vertices[2].tex_coord.x );
    glVertex3dv( &vertices[2].position.x);
#endif

    glEnd();

    if ( materials_nonnull )
        vertices[0].material->reset_gl_state();
}
} /* _462 */
