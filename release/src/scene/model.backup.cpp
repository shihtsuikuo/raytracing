/**
 * @file model.cpp
 * @brief Model class
 *
 * @author Eric Butler (edbutler)
 * @author Zeyang Li (zeyangl)
 */

#include "scene/model.hpp"
#include "scene/material.hpp"
#include "application/opengl.hpp"
#include "scene/triangle.hpp"
#include <iostream>
#include <cstring>
#include <string>
#include <fstream>
#include <sstream>
#define SQUARE_MAX_DISTANCE 100000000;

namespace _462 {

Model::Model() : mesh( 0 ), material( 0 ) { }
Model::~Model() { }

/****** stkuo added ******/
/*void getBarycentric(const Vector3& point, const Vector3& va, const Vector3& vb, const Vector3& vc, Vector3& bary_coord){
	Vector3 v0 = vb - va, v1 = vc - va, v2 = point - va;
	real_t d00 = dot(v0, v0);
	real_t d01 = dot(v0, v1);
	real_t d11 = dot(v1, v1);
	real_t d20 = dot(v2, v0);
	real_t d21 = dot(v2, v1);
	real_t denom = d00 * d11 - d01 * d01;
	bary_coord.y = (d11 * d20 - d01 * d21) / denom;
	bary_coord.z = (d00 * d21 - d01 * d20) / denom;
        bary_coord.x = 1.0f - bary_coord.y - bary_coord.z;
}*/

GeoType Model::getGeoType(){
	return GeoType::MODEL;
}

Intersection Model::getIntersec(Ray r){
	Intersection intersec;
        intersec.hit = false;
	//intersec.t = 0;
	intersec.hitPoint_world = Vector3::Zero();
	intersec.hitNormal_world = Vector3::Zero();
        Ray convertedRay;
	convertedRay.e = (this->invMat * Vector4(r.e, 1)).xyz();
	convertedRay.d = (this->invMat * Vector4(r.d, 0)).xyz();
	real_t t, sqr_minDistance = SQUARE_MAX_DISTANCE;
	const MeshTriangle* triangleList;
	const MeshVertex* vertexList;
	if(this->mesh->num_triangles() != 0){
		triangleList = this->mesh->get_triangles();
		vertexList = this->mesh->get_vertices();
		Vector3 triNormal, point, bary_coord;
		Vector3 v0, v1, v2;
		for(size_t i = 0; i<this->mesh->num_triangles(); i++){
			v0 = vertexList[triangleList[i].vertices[0]].position;
			v1 = vertexList[triangleList[i].vertices[1]].position;
			v2 = vertexList[triangleList[i].vertices[2]].position;
			triNormal = normalize(cross((v1-v0), (v2-v0)));
			// plane formula:  dot(((e+dt)-v0), triNormal) = 0
			real_t denominator, numerator;
			denominator = -(triNormal.x * convertedRay.d.x
				      + triNormal.y * convertedRay.d.y
				      + triNormal.z * convertedRay.d.z);
			numerator = triNormal.x * (convertedRay.e.x - v0.x)
				  + triNormal.y * (convertedRay.e.y - v0.y)
				  + triNormal.z * (convertedRay.e.z - v0.z);
			t = numerator/denominator;
			//check if it is on plane
			point = convertedRay.e + (t*convertedRay.d);
			if(insideTriangle(point, v0, v1, v2)){
				if(dot(convertedRay.d, triNormal) < 0){  //correct normal direction
					real_t sqr_tempDistance = squared_length(point - convertedRay.e);
					if(sqr_tempDistance < sqr_minDistance){
						intersec.hit = true;
						intersec.t = t;		
						intersec.hitPoint_world = (this->mat * Vector4(point, 1)).xyz();
                        			intersec.hitNormal_world = (this->normMat * triNormal);
						//intersec.hitUVCoord = this->material.texture.sample()
						//intersec.back = false;
						sqr_minDistance = sqr_tempDistance;
					}
				}/*else{
					intersec.hit = false;
					intersec.t = t;
					intersec.back = true;
					
				}*/
			}
				 
		}
	}
        return intersec;
}

/***** end of stkuo  *****/


void Model::render() const
{
    if ( !mesh )
        return;
    if ( material )
        material->set_gl_state();
    mesh->render();
    if ( material )
        material->reset_gl_state();
}
bool Model::initialize(){
    Geometry::initialize();
    return true;
}

} /* _462 */
