/**
 * @file raytacer.cpp
 * @brief Raytracer class
 *
 * Implement these functions for project 4.
 *
 * @author H. Q. Bovik (hqbovik)
 * @bug Unimplemented
 */

#include "raytracer.hpp"
#include "scene/sphere.hpp"
#include "scene/model.hpp"
#include "scene/triangle.hpp"
#include "scene/scene.hpp"
#include "math/quickselect.hpp"
#include "p3/randomgeo.hpp"
#include "p3/util.hpp"
#include <SDL_timer.h>
#include <cmath>
#define SQUARE_MAX_DISTANCE 1000000
#define SLOP_FACTOR 0.0001 
#define MAX_RAY_DEPTH 6
#define DO_REFLECTION 1
#define DO_REFRACTION 0
#define DEPTH_OF_FIELD 1
#define BLUR_LENGTH 0 //0.01
#define GLOSSY_RANGE 0.03 //better effect range: [0.01, 0.1]
namespace _462 {


//number of rows to render before updating the result
static const unsigned STEP_SIZE = 1;
static const unsigned CHUNK_SIZE = 1;

Raytracer::Raytracer() {
        scene = 0;
        width = 0;
        height = 0;
    }

Raytracer::~Raytracer() { }

inline Color3 getAttenuatedLight(const SphereLight& light, Vector3& position){
	Color3 ret = light.color;
	real_t d = length(light.position-position) - light.radius;
	ret.r /= light.attenuation.constant + d*light.attenuation.linear + d*d*light.attenuation.quadratic;
	ret.g /= light.attenuation.constant + d*light.attenuation.linear + d*d*light.attenuation.quadratic;
	ret.b /= light.attenuation.constant + d*light.attenuation.linear + d*d*light.attenuation.quadratic;
	return ret;	
}

void getBarycentric(const Vector3& point, const Vector3& va, const Vector3& vb, const Vector3& vc, Vector3& bary_coord){
        Vector3 v0 = vb - va, v1 = vc - va, v2 = point - va;
        real_t d00 = dot(v0, v0);
        real_t d01 = dot(v0, v1);
        real_t d11 = dot(v1, v1);
        real_t d20 = dot(v2, v0);
        real_t d21 = dot(v2, v1);
        real_t denom = d00 * d11 - d01 * d01;
        bary_coord.y = (d11 * d20 - d01 * d21) / denom;
        bary_coord.z = (d00 * d21 - d01 * d20) / denom;
        bary_coord.x = 1.0f - bary_coord.y - bary_coord.z;
}

Vector3 getSampleLight(const SphereLight& light){
	Vector3 p;
	real_t x = random_gaussian();
	real_t y = random_gaussian();
	real_t z = random_gaussian();
	p = Vector3(x, y, z);
	p /= length(p);
	p *= light.radius;
	p += light.position;
	return p;
}

bool schlick(real_t f){
	if(rand()%1000 < f*1000) 
		return DO_REFLECTION;
	return DO_REFRACTION;
}
/**
 * Initializes the raytracer for the given scene. Overrides any previous
 * initializations. May be invoked before a previous raytrace completes.
 * @param scene The scene to raytrace.
 * @param width The width of the image being raytraced.
 * @param height The height of the image being raytraced.
 * @return true on success, false on error. The raytrace will abort if
 *  false is returned.
 */
bool Raytracer::initialize(Scene* scene, size_t num_samples,
               size_t width, size_t height)
{
    this->scene = scene;
    this->num_samples = num_samples;
    this->width = width;
    this->height = height;

    current_row = 0;

    projector.init(scene->camera);
    scene->initialize();
    photonMap.initialize(scene);
    return true;
}



//compute ambient lighting
Color3 Raytracer::trace_ray(Ray &ray, size_t depth, real_t currentRIndex/*maybe some more arguments*/){
    //TODO: render something more interesting
    if(squared_length(ray.d) != 1) ray.d = normalize(ray.d);
    Intersection intersec;
    intersec.hit = false;
    intersec.t = 0;
    intersec.hitPoint_world = Vector3::Zero();
    intersec.hitNormal_world = Vector3::Zero();
    intersec.hitPoint_world = Vector3::Zero();
    intersec.rIndex = 0;
    size_t g_num = this->scene->num_geometries();
    real_t sqr_minDistance = SQUARE_MAX_DISTANCE;
    if(g_num == 0 ) return this->scene->background_color;
    Geometry* const* gList = this->scene->get_geometries();
    for(size_t g = 0; g<g_num; g++){
	Intersection tryIntersec;
	gList[g]->getIntersec(ray, tryIntersec);
	if(tryIntersec.hit == true && tryIntersec.t > SLOP_FACTOR){
		//real_t temp_sqrDistance = squared_length(ray.atTime(intersec.t) - ray.e); 
		real_t temp_sqrDistance = squared_length(tryIntersec.hitPoint_world - ray.e); 
		if(temp_sqrDistance < sqr_minDistance){
			sqr_minDistance = temp_sqrDistance;
			intersec = tryIntersec;
		}
	}
		
    }
    intersec.hitNormal_world = normalize(intersec.hitNormal_world);
    if(intersec.hit == false)
    	return this->scene->background_color;
   
 
    Color3 amb = Color3::Black();
    Color3 reflec = Color3::Black();
    Color3 refrac = Color3::Black();
    Color3 dirIllum = Color3::Black();
    real_t rIndex = intersec.rIndex;
    if(depth < MAX_RAY_DEPTH){   //keep tracing, do reflection or refraction
		//fresnel the 3rd parameter shoud be dot(ray.d, intersec.hitnormal_world).length(ray.d), but ray.d is normalized
		bool operation = schlick(fresnel(currentRIndex, rIndex, dot(-ray.d,intersec.hitNormal_world) )); 
 		if(rIndex == 0 || operation == DO_REFLECTION){
			Ray reflectionRay;
			reflectionRay.e = intersec.hitPoint_world;
			if(dot(intersec.hitNormal_world, ray.d) < 0)
				reflectionRay.d = reflect(intersec.hitNormal_world, ray.d, GLOSSY_RANGE); 
			else
				reflectionRay.d = reflect(-intersec.hitNormal_world, ray.d, GLOSSY_RANGE); 
			reflec = (trace_ray(reflectionRay, depth+1, currentRIndex))*intersec.specular;
		}else if (rIndex != 0){
			Ray refractionRay;
			refractionRay.e = intersec.hitPoint_world;
			if(dot(intersec.hitNormal_world, ray.d) < 0)
				refractionRay.d = refract(intersec.hitNormal_world, ray.d, currentRIndex/rIndex);
			else
				refractionRay.d = refract(-intersec.hitNormal_world, ray.d, currentRIndex/rIndex);
			if(refractionRay.d == Vector3::Zero()){ //total reflection!
				printf("b");
				Ray reflectionRay;
				reflectionRay.e = intersec.hitPoint_world;
				if(dot(intersec.hitNormal_world, ray.d) < 0)
					reflectionRay.d = reflect(intersec.hitNormal_world, ray.d, 0); 
				else
					reflectionRay.d = reflect(-intersec.hitNormal_world, ray.d, 0); 
				//still belong to refraction
				refrac = (trace_ray(reflectionRay, depth+1, currentRIndex))*intersec.specular;
			}else{
				refrac = trace_ray(refractionRay, depth+1, rIndex);
			}
		}
    }
    if(rIndex == 0){
		amb = (this->scene->ambient_light * intersec.ambient);
		dirIllum += amb;
		Ray shadowRay;
		shadowRay.e = intersec.hitPoint_world;
		const SphereLight* lights = this->scene->get_lights();
		for(size_t i = 0; i<this->scene->num_lights(); i++){
			Vector3 sampledLightPosition = getSampleLight(lights[i]);
			shadowRay.d = normalize(sampledLightPosition - shadowRay.e);
			bool inShadow = false;
			real_t distance2Light = squared_length(sampledLightPosition - shadowRay.e);
			for(size_t g = 0; g<g_num; g++){
				Intersection tryIntersec;
				gList[g]->getIntersec(shadowRay, tryIntersec);
				real_t distance2Obstacle = squared_length(tryIntersec.hitPoint_world - shadowRay.e);
				if(tryIntersec.hit == true && tryIntersec.t > SLOP_FACTOR && distance2Light > distance2Obstacle){
					inShadow = true;
					break;
				}
			}
			if(inShadow == true) // this light is blocked
				continue;
			Color3 att_color = getAttenuatedLight(lights[i], shadowRay.e);
			dirIllum += att_color * intersec.diffuse * fmax( dot(intersec.hitNormal_world, shadowRay.d), 0.0f);	
		}
    }
	//start dealing with direct illumination
    Color3 tp = Color3::White();
    tp = intersec.texture_color;
    return tp*(dirIllum+reflec+refrac);
    
    //return Color3(fabs(sin(10*ray.d.x)),fabs(10*cos(ray.d.y)),fabs(10*tan(ray.d.y)));
}

/**
 * Performs a raytrace on the given pixel on the current scene.
 * The pixel is relative to the bottom-left corner of the image.
 * @param scene The scene to trace.
 * @param x The x-coordinate of the pixel to trace.
 * @param y The y-coordinate of the pixel to trace.
 * @param width The width of the screen in pixels.
 * @param height The height of the screen in pixels.
 * @return The color of that pixel in the final image.
 */
Color3 Raytracer::trace_pixel(size_t x,
                  size_t y,
                  size_t width,
                  size_t height)
{
    assert(x < width);
    assert(y < height);

    real_t dx = real_t(1)/width;
    real_t dy = real_t(1)/height;

    Color3 res = Color3::Black();
    unsigned int iter;
    for (iter = 0; iter < num_samples; iter++)
    {
        // pick a point within the pixel boundaries to fire our
        // ray through.
        real_t i = real_t(2)*(real_t(x)+random_uniform())*dx - real_t(1);
        real_t j = real_t(2)*(real_t(y) + random_uniform())*dy - real_t(1);
	
        
	Ray r = Ray(scene->camera.get_position(), projector.get_pixel_dir(i, j));
    	
	if(BLUR_LENGTH != 0){
		r.e = lense(r.e, BLUR_LENGTH, scene->camera.get_direction());
	}
	
        res += trace_ray(r, 0, scene->refractive_index);
	
        // TODO return the color of the given pixel
        // you don't have to use this stub function if you prefer to
        // write your own version of Raytracer::raytrace.

    }
    return res*(real_t(1)/num_samples);
}

/**
 * Raytraces some portion of the scene. Should raytrace for about
 * max_time duration and then return, even if the raytrace is not copmlete.
 * The results should be placed in the given buffer.
 * @param buffer The buffer into which to place the color data. It is
 *  32-bit RGBA (4 bytes per pixel), in row-major order.
 * @param max_time, If non-null, the maximum suggested time this
 *  function raytrace before returning, in seconds. If null, the raytrace
 *  should run to completion.
 * @return true if the raytrace is complete, false if there is more
 *  work to be done.
 */
bool Raytracer::raytrace(unsigned char* buffer, real_t* max_time)
{
    
    static const size_t PRINT_INTERVAL = 64;

    // the time in milliseconds that we should stop
    unsigned int end_time = 0;
    bool is_done;

    if (max_time)
    {
        // convert duration to milliseconds
        unsigned int duration = (unsigned int) (*max_time * 1000);
        end_time = SDL_GetTicks() + duration;
    }

    // until time is up, run the raytrace. we render an entire group of
    // rows at once for simplicity and efficiency.
    for (; !max_time || end_time > SDL_GetTicks(); current_row += STEP_SIZE)
    {
        // we're done if we finish the last row
        is_done = current_row >= height;
        // break if we finish
        if (is_done) break;

        int loop_upper = std::min(current_row + STEP_SIZE, height);

        for (int c_row = current_row; c_row < loop_upper; c_row++)
        {
            /*
             * This defines a critical region of code that should be
             * executed sequentially.
             */
#pragma omp critical
            {
                if (c_row % PRINT_INTERVAL == 0)
                    printf("Raytracing (Row %d)\n", c_row);
            }
            
        // This tells OpenMP that this loop can be parallelized.
#pragma omp parallel for schedule(dynamic, CHUNK_SIZE)
            for (size_t x = 0; x < width; x++)
            {
                // trace a pixel
                Color3 color = trace_pixel(x, c_row, width, height);
                // write the result to the buffer, always use 1.0 as the alpha
                color.to_array4(&buffer[4 * (c_row * width + x)]);
            }
#pragma omp barrier

        }
    }

    if (is_done) printf("Done raytracing!\n");

    return is_done;
}

} /* _462 */
