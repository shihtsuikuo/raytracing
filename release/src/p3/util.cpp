#include "p3/util.hpp"
namespace _462{


//ensures that the light has a maximum intensity of 1, and in order
//to cancel this scaling factor, returns the probability of continuing.
real_t montecarlo(Color3& light){
    real_t factor=std::max(light.r,std::max(light.g,light.b));
    light*=1/factor;
    return factor;
}

Vector3 reflect(Vector3 norm, Vector3 inc, real_t glossy){
	Vector3 out;
	Vector3 u, v;
	out = inc - 2.f * dot(inc, norm) * norm;
	if(squared_length(out) != 1) 
		out = normalize(out);
	if(glossy != 0){
		Vector3 tmp;
		tmp = out;
		if(fabs(tmp.x) < fabs(tmp.y)){
			if(fabs(tmp.x) < fabs(tmp.z)) tmp.x = 1;
			else tmp.z = 1; 
		}else{
			if(fabs(tmp.y) < fabs(tmp.z)) tmp.y = 1;
			else tmp.z = 1;
		}
		u = normalize(cross(tmp, out));
		v = cross(u, out);

		real_t rand_u = -(glossy/2) + random_gaussian() * glossy;
        	real_t rand_v = -(glossy/2) + random_gaussian() * glossy;
		
		Vector3 _out = out + rand_u*u + rand_v*v;
		//if(out != _out) printf("!");
		if(dot(_out, norm) > 0) return _out;
		else return out;

	}
	return squared_length(out) == 1 ? out : normalize(out);
	//return out;    
}

Vector3 refract(Vector3 norm, Vector3 inc, real_t ratio){
	
	Vector3 out;
	real_t NdotI = dot(inc, norm);
  	real_t k = 1.f - ratio * ratio * (1.f - NdotI * NdotI);
  	if (k < 0.f)
    		out = Vector3::Zero();
  	else
    		out = ratio * inc - (ratio * NdotI + sqrtf(k)) * norm;

	return squared_length(out) == 1 ? out : normalize(out);
}

real_t fresnel(real_t rIndex_old, real_t rIndex_new, real_t cos_theta){
	real_t R0 = (rIndex_new-rIndex_old)/(rIndex_new+rIndex_old) *  (rIndex_new-rIndex_old)/(rIndex_new+rIndex_old);
	real_t _1cos05 = pow((1-cos_theta), 5);
	return R0+((1-R0)*_1cos05);

}
	
Vector3 lense(Vector3 center, real_t length, Vector3 dir){
	Vector3 pos;
	Vector3 tmp, u, v;
                tmp = dir;
                if(fabs(tmp.x) < fabs(tmp.y)){
                        if(fabs(tmp.x) < fabs(tmp.z)) tmp.x = 1;
                        else tmp.z = 1;
                }else{
                        if(fabs(tmp.y) < fabs(tmp.z)) tmp.y = 1;
                        else tmp.z = 1;
                }
                u = normalize(cross(tmp, dir));
                v = cross(u, dir);

                real_t rand_u = -(length/2) + random_gaussian() * length;
                real_t rand_v = -(length/2) + random_gaussian() * length;

		pos = center + rand_u*u + rand_v*v;
                //Vector3 _out = out + rand_u*u + rand_v*v;

//	pos.x += -(length/2) + random_gaussian() * length;
//	pos.y += -(length/2) + random_gaussian() * length;

	return pos;

}

}
