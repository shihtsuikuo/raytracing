# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Ariel_K/Downloads/release/src/p3/main.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/main.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/neighbor.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/neighbor.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/photon.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/photon.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/photonmap.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/photonmap.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/randomgeo.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/randomgeo.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/raytracer.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/raytracer.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/p3/util.cpp" "/Users/Ariel_K/Downloads/release/build/p3/CMakeFiles/p3.dir/util.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/Users/Ariel_K/Downloads/release/build/application/CMakeFiles/application.dir/DependInfo.cmake"
  "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/DependInfo.cmake"
  "/Users/Ariel_K/Downloads/release/build/scene/CMakeFiles/scene.dir/DependInfo.cmake"
  "/Users/Ariel_K/Downloads/release/build/tinyxml/CMakeFiles/tinyxml.dir/DependInfo.cmake"
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Ariel_K/Downloads/release/src"
  "/usr/local/include/SDL"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
