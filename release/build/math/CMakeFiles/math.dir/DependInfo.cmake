# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/Users/Ariel_K/Downloads/release/src/math/axis.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/axis.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/camera.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/camera.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/color.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/color.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/math.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/math.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/matrix.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/matrix.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/quaternion.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/quaternion.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/random462.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/random462.cpp.o"
  "/Users/Ariel_K/Downloads/release/src/math/vector.cpp" "/Users/Ariel_K/Downloads/release/build/math/CMakeFiles/math.dir/vector.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
set(CMAKE_C_TARGET_INCLUDE_PATH
  "/Users/Ariel_K/Downloads/release/src"
  "/usr/local/include/SDL"
  "/usr/local/include"
  )
set(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
set(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
